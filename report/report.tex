\documentclass{article}[12pt]

% useful packages
\usepackage{fullpage}
\usepackage{amsmath,amssymb,amsthm,amsfonts}
\usepackage{graphicx}
\usepackage{enumerate}
\usepackage{algorithm,algorithmic}
\usepackage{xcolor}
\usepackage{bbm}
\usepackage{hyperref}
\usepackage{caption,subcaption}
\usepackage{placeins}


% theorem type environments
\newtheorem{thm}{Theorem}
\newtheorem{prop}{Proposition}
\newtheorem{lemma}{Lemma}
\newtheorem{cor}{Corollary}
\newtheorem{defn}{Definition}
\newtheorem{assump}{Assumption}
\newtheorem{example}{Example}
\newtheorem{conjecture}{Conjecture}

% frequently used symbols
\newcommand{\bE}{\mathbb{E}}
\newcommand{\bP}{\mathbb{P}}
\newcommand{\bQ}{\mathbb{Q}}
\newcommand{\bR}{\mathbb{R}}
\newcommand{\bS}{\mathbb{S}}
\newcommand{\bN}{\mathbb{N}}
\newcommand{\bZ}{\mathbb{Z}}
\newcommand{\sC}{{\mathcal C}} 
\newcommand{\sD}{{\mathcal D}} 
\newcommand{\sE}{{\mathcal E}} 
\newcommand{\sF}{{\mathcal F}} 
\newcommand{\sL}{{\mathcal L}} 
\newcommand{\sH}{{\mathcal H}} 
\newcommand{\sN}{{\mathcal N}} 
\newcommand{\sO}{{\mathcal O}} 
\newcommand{\sP}{{\mathcal P}} 
\newcommand{\sR}{{\mathcal R}} 
\newcommand{\sS}{{\mathcal S}}
\newcommand{\sU}{{\mathcal U}} 
\newcommand{\sX}{{\mathcal X}} 
\newcommand{\sY}{{\mathcal Y}} 
\newcommand{\sZ}{{\mathcal Z}}

% operators
\newcommand{\sign}{\mathop{\mathrm{sign}}}
\newcommand{\supp}{\mathop{\mathrm{supp}}} % support
\newcommand{\argmin}{\operatornamewithlimits{arg\ min}}
\newcommand{\argmax}{\operatornamewithlimits{arg\ max}}
\newcommand{\dist}{\operatorname{dist}}
\newcommand{\tr}{\text{tr}}
\newcommand{\vecop}{\text{vec}}
\newcommand{\st}{\operatorname{s.t.}}
\newcommand{\cut}{\setminus}
\newcommand{\ind}[1]{\mathbbm{1}\left\{#1\right\}} 
\newcommand{\given}{\ | \ }

% grouping operators
\newcommand{\brac}[1]{\left[#1\right]}
\newcommand{\set}[1]{\left\{#1\right\}}
\newcommand{\abs}[1]{\left\lvert #1 \right\rvert}
\newcommand{\paren}[1]{\left(#1\right)}
\newcommand{\norm}[1]{\left\|#1\right\|}
\newcommand{\ip}[2]{\left\langle #1,#2 \right\rangle}

% code commands
\newcommand{\matlab}{\textsc{Matlab }}

% header command
\newcommand{\header}[2]{
    \pagestyle{myheadings}
    \thispagestyle{plain}
    \newpage
    \setcounter{page}{1}
    \setlength{\headsep}{10mm}
    \noindent
    \begin{center}
    \framebox{
        \vbox{\vspace{2mm}
            \hbox to 6.28in { {\bf EE 510: Mathematical Foundations of Machine Learning
            \hfill Winter \the\year} }
        \vspace{4mm}
        \hbox to 6.28in { {\Large \hfill Course Project: #1 \hfill} }
        \vspace{2mm}
        \hbox to 6.28in { \Large \hfill Due: March 16, 2022, 11:59PM PT \hfill }
        \vspace{2mm}
        \hbox to 6.28in { {\it Student Names: #2 \hfill}}
        \vspace{2mm}}
   }
   \end{center}
   %\markboth{Homework #1}{Homework #1}
   \vspace*{4mm}
}

\begin{document}

%% PLEASE REPLACE THE TEXT BELOW WITH YOUR OWN PROJECT TOPIC & NAMES
\header{Nonlinear Dimensionality Reduction}{Aakarsh Nair, Aalap Khanolkar, Connor Robetorye}
%% PLEASE REPLACE THE TEXT ABOVE WITH YOUR OWN PROJECT TOPIC & NAMES
\begin{figure}[htbp!]
	\centering
	\includegraphics[width=.3\textwidth]{./images/ant-manifold.jpeg}
	\caption{MC Escher - Ants on a locally linear manifold}
	\label{ants-manifold}
\end{figure}


\section{Paper Descriptions}
\iffalse
\label{sec:descriptions}

All individual paper descriptions go in this section. Details on what should be in each section are in Section \ref{sec:paper1}.
Note that I am including the \texttt{bibliography.bib} file so you can learn to cite papers like this \cite{elhamifar2013sparse}. The easy way to do this is to go to Google Scholar, search the paper name, and click the quotation mark icon. There is a link to the BibTeX entry that you can simply copy and paste into your \texttt{bibliography.bib} file.


A great way to work on the paper collaboratively is to copy and paste this template into \href{www.overleaf.com}{Overleaf}, with each student having access to the document and updates appearing in real time. For those familiar with Git: Overleaf has great Git integration as well, enabling individuals to work on sections on their own computer and push to the group document. You may ask me more on this if you have questions on set-up.

Lastly, if desired, you may replicate this general template in another word processing document. Please turn in your project report in PDF format regardless of which text processing interface you use.
\fi
\subsection{T-SNE}
\label{sec:paper1}

\textbf{Paper Title:} Visualizing Data using t-SNE \\
\textbf{Student Name:} Aakarsh Nair

\subsubsection{Problem Description \& Formulation}

Each team member should write their own problem description. You may (and should) discuss the problem among yourselves, but I want each of you to write your own individual description of the problem.

\begin{itemize}
	\item Be sure to give both a verbal/intuitive problem description as well as a mathematical description and formulation of what the proposed algorithm solves.	
	\item Talk about why this problem is important, either from a practical or theoretical perspective (or both).
	\item Discuss what gaps exist in the literature and how this paper fills those gaps. What is the novel contribution of this paper, and why is it important?
	\item Discuss limitations of the method / algorithm. In particular, note any assumptions that must be made on the data in order for the method to work as described. Discuss any cases that you can think of where this method should \textbf{not} be used. Think about factors such as data noise, uneven distributions, etc. 
	\item Do \textbf{not} simply copy the problem description from your selected paper. This is plagiarism. Your description should be your own.
	\item In this and all sections, be sure to define every variable used, as well as any non-standard sets, objects, or functions/operations.
\end{itemize}

\textbf{Answer:}

If we assume that most real world data even though it lies in a very high dimensional space tends not 
to fill the space but instead lies on a low dimensional manifold in this high dimensional space, 
then the task of  non-linear dimensionality reduction is to find the best description of this manifold. This is a reasonable assumption as most high dimensional data can be thought of as being generated physical processes on the basis of few parameters being modified continuously. High-dimensional maybe locally 
linear while still lying on a non-linear surface manifold. See Figure \ref{ants-manifold} ants locally exploring the 3-dimensional manifold.

The paper provides for a way to visualize high dimensional data on a 2 or 3 dimensional map in order to be 
able to discern the structure of the data at a variety of scales. It is an improvement  on previous techniques 
where there might crowd data when visualized on the low dimensional map. It works under the assumption 
that high dimensional data lies on several different but related low-dimensional manifolds. 

Similar data points in the high dimensional space are mapped to the similar data points in the low-dimensional space while retaining as much of the global and local structure fo the data. As t-SNE mapping is non-linear it has greater freedom to map manifolds without having to rigidly preserve pairwise distances. Thus it is able  to more easily model high-dimensional manifolds in lower dimensions.   The visualization it produces can be useful in debugging of algorithms producing high-dimensional output and providing intuition for hyper-parameter 
selection for such algorithms. Some high lighted applications come in fields like biology, where they t-SNE has been used to visualize FMRI \cite{du2015group}, in cytometry for analysis of cell data \cite{toghi2019quantitative} as well as visualizing  activations in various layers of convolutional
neural networks \cite{rusu2015policy}. 

While the t-SNE is can provide good human interpretable visualizations the non-linear and stochastic 
nature of the algorithms can mean that it has problems scaling for very large datasets. The algorithm 
is also susceptible to requiring additional tuning for hyper parameters. The use of conjugate gradient can be sensitive to initial conditions making it harder to provide exact repeatable visualizations. 


\subsubsection{Algorithm Description}


\begin{itemize}
	\item What type of algorithm is this? Does it use iterative methods? Optimization? Geometric intuition?
	\item Why should this algorithm work? Provide intuition beyond what the theoretical results can tell us.
	\item Does this algorithm make any assumptions on data? Think about and discuss how factors such as noise, uneven distributions, and correlations between data sources (if the method can be used on multivariate data) may influence the results.  
	\item What tools or methods does the algorithm leverage? Is it applying a known formulation to this specific problem? Is it leveraging some geometric insight to simplify the problem?
	\item Does the algorithm perform well empirically? In what cases?
	\item What (if any) topics from class are used in the algorithm, either in its development or in the actual pseudo-code?
\end{itemize}

\textbf{Answer:}

The algorithms  begins by defining a metric for similarity of two points $i$  and $j$ in the
 high-dimensional space. Such that 

\begin{equation}
	p_{j|i} = \frac{exp(-||x_i - x_j||^{2}/2\sigma_i^{2})}{\sum_{j' \neq i} exp(-||x_k - x_l||^{2}/2\sigma_{i}^{2})}
\end{equation}

Where $p_{j|i}$ is the probability that a given point in the high dimensional space centered at the point $i$ has as its neighbor the point $j$,  with the value of $\sigma_i$ determined based of density of points in the neighborhood of $i$ such that sparse neighborhoods get larger values of $\sigma_i$. 

The locations of the points on the two dimensional map are modeled using the Student t-distribution/Cauchy distribution. The heavy tailed nature of this distribution.


In this section, you should give an intuitive explanation of the algorithm. Below are some prompts that may be helpful.


\subsubsection{Theoretical Results}

In this section, describe the theoretical results that accompany the algorithm (if any). Note that I do not expect you to understand the proofs (or even the more technical results) in detail. The main goal is to get a clear picture of what can be said about the algorithm.
\begin{itemize}
    \item What assumptions are made for the theoretical results to hold?
    \item Do the results guarantee that the problem of interest is solved? Under what conditions?
    \item Do the results guarantee that the algorithm converges? To a local or global minimum?
\end{itemize}

\subsubsection{Relation to Course Material}

In this section, you should discuss \emph{any} aspects of the paper that relate to the course material. Consider the introduction, problem statement, algorithm, theoretical results, empirical results, and any conclusions.

\begin{itemize}
    \item What (if any) topics from class are used in the algorithm, either in its development or in the actual pseudo-code?
    \item What (if any) topics from class are used in the theoretical results?
    \item Are there any portions of the paper that you could not have understood prior to taking this course that now make (at least some) sense to you?
    \item What aspects of the paper do you still not understand? Are these beyond the scope of ECE 510, or are they parts of the class that you still don't understand? Be honest please :)
\end{itemize}

\subsection{Paper 2}
\label{sec:paper2}

\textbf{Paper Title:} Title goes here \\
\textbf{Student Name:} Student name goes here

Same subsections as Section \ref{sec:paper1}.

\subsection{Paper 3}
\label{sec:paper3}

\textbf{Paper Title:} Title goes here \\
\textbf{Student Name:} Student name goes here

Same subsections as Section \ref{sec:paper1}.

\section{Comparison of Algorithms}

In this section, your group should compare the following merits of the papers.
\begin{itemize}
    \item Interpretability. Is any single algorithm simpler to understand or implement? This may seem silly, but it is often a determining factor in which algorithms are actually used in practice.
    \item Theoretical guarantees. Does any paper have the strongest theoretical guarantees? Does one algorithm work in certain cases, while another works in other cases?
    \item Empirical guarantees. Does any paper have the strongest empirical results? Does one algorithm work in certain cases, while another works in other cases? Consider both synthetic and real/benchmark datasets if available.
    \item Computational complexity. Discuss the computational complexity of each algorithm. What problem parameters does it scale with? Are there regimes in which one algorithm should be chosen over another?
\end{itemize}

\section{Algorithm Implementation \& Testing}

Instructions below. Do not feel obligated to run exactly the same tests as in the paper. These often take a large amount of computation time.
\begin{itemize}
    \item Clearly state which algorithm you've implemented and why you chose this algorithm.
    \item Include your algorithm code either as an appendix at the end of the paper or in some digital perform if preferred (e.g., a Jupyter notebook or GitHub repository).
\end{itemize}

\subsection{Results on Synthetic Data}


As the performance of T-SNE can only 

\iffalse
In this section, include a few plots of performance on synthetic data. You may choose to simulate scenarios similar to those in the corresponding paper. Groups of three should do at least one plot. Groups of four should do at least two and try to find the limits of where the algorithm performs well. Some plots of interest may be
\begin{itemize}
    \item Performance vs. noise level
    \item Performance vs. problem size (heatmap if applicable)
    \item Performance vs. tuning parameters (heatmap if applicable)
    \item Run time vs. problem size
\end{itemize}

\fi
\begin{figure}[h!]
	\centering
	\includegraphics[width=.5\textwidth, keepaspectratio]{../t-sne/images/tsne_swiss_roll_reference_n_samples_5000_perplexity_10_learning_rate_50.png}
	\caption{Sample run on the swiss roll dataset, after optimizing hyper parameters the dimensionality reduction does seem ot show some form of the manifold}
\end{figure}

\FloatBarrier

\subsection{Results on Benchmark Data}

Choose at least one benchmark dataset where it makes sense to apply your algorithm. You may choose one from among the papers your group has read or find a different dataset. Be sure to cite the appropriate authors or location for your dataset. 
\begin{itemize}
    \item Report the performance of your implemented algorithm. 
    \item Report the run time for your algorithm on this dataset.
    \item You may either report the best performance you were able to obtain and the accompanying algorithm parameters or plot the performance vs. tuning parameters.
    \item Does your performance match what is reported in the paper? The answer will almost certainly be no. Why do you think this is?
\end{itemize}

\bibliographystyle{IEEEtran}
\bibliography{bibliography}

\appendix

\section{Appendix}

This is an appendix where you may include your code.

\end{document}
