import math
from src.t_sne import perplexity_row
from src.tsne_ref_2 import calc_perplexity
from src.t_sne import compute_point_sigma
from src.t_sne import is_symmetric_matrix
from src.t_sne import compute_original_similairty
from src.t_sne import vectorized_distance_matrix
from src.t_sne import perplexity
from src.t_sne import simple_distance_matrix
from sklearn.datasets import make_swiss_roll
import numpy as np

def assert_distance_matrix_has_zero_diagonals(M):
    """
    In the distance matrix all the diagonal entries for the
    points must be zero.
    """
    assert np.isclose(np.linalg.norm(np.diag(M)), 0.0)

def assert_distance_function_produces_zero_diagonals(calculate_distance_matrix):
    n_samples = 100
    X, color = make_swiss_roll(n_samples)
    M = calculate_distance_matrix(X)
    assert_distance_matrix_has_zero_diagonals(M)

def assert_distance_function_is_valid_for_identity_matrix(calculate_distance_matrix):
    n,d = 100, 100
    s = n, d
    I_100 = np.eye(n, d)
    D = calculate_distance_matrix(I_100)
    assert_distance_matrix_has_zero_diagonals(D)
    D_s = np.broadcast_to(math.sqrt(2), s) - (np.eye(n)*math.sqrt(2))
    assert np.allclose(D - D_s, 0)

def assert_distance_matrix_for_swissroll_has_zero_diagonals(calculate_distance_matrix):
    n_samples = 100
    X, color = make_swiss_roll(n_samples)
    M = calculate_distance_matrix(X)
    assert_distance_matrix_has_zero_diagonals(M)

def test_distance_matrix_for_identity():
    assert_distance_function_is_valid_for_identity_matrix(simple_distance_matrix)

def test_euclidean_computation():
    """ We can test the euclidean distance computation. """
    assert_distance_matrix_for_swissroll_has_zero_diagonals(simple_distance_matrix)

def test_vectorized_distance_matrix_and_simple_distance_matrix_return_same_data():
    """
    Assert that the vectorized distance_matrix
    and the simple_distance_matrix returns
    the same results.
    """
    I_100 = np.eye(100)
    M = simple_distance_matrix(I_100)
    M_prime = vectorized_distance_matrix(I_100)
    assert is_symmetric_matrix(M)
    assert np.allclose(M, M_prime, 1e-8, 1e-8)


def test_compute_point_sigma():
    I_100 = np.eye(100)
    M_prime = vectorized_distance_matrix(I_100)
    sigma = compute_point_sigma(M_prime[1], 2, target_perplixity=5)
    print("SIGMA: ", sigma)

"""
def test_neg_square_euc_dist():
    n_samples = 100
    X, color = make_swiss_roll(n_samples)
    print("X_SHAPE", np.shape(X))
    M = neg_squared_euc_dists(X)
    M_prime = simple_distance_matrix(X)
    M_square = np.square(M_prime)
    print("M", M)
    print("M_prime", M_prime)
    print("M_square", M_square)
    # assert_distance_matrix_has_zero_diagonals(M)
"""
"""
def test_neg_square_identity():
   I = np.eye(3)
   d = neg_squared_euc_dists(I)
   print("D", d)
"""

def test_perplexity_of_zeros():
    """
    Equal probability matirx
    :return:
    """
    n = 100
    p = 1/n
    probability_matrix  = np.broadcast_to(p, (n,n)) - (np.eye(n)* p) + (np.eye(n) * 1e-14)
    print("PROBABILITY_MATRIX", probability_matrix)
    perp = perplexity(probability_matrix, 0)
    print("perplexity", perp)
    cp = calc_perplexity(probability_matrix)
    print("calculated_perplexity", cp)

def test_perplexity_computation_comparison():
    """
    Compute the perlexity given a set of probabilities
    """
    half_half = np.array([0.5, 0.5, 0.5, 0.5])
    half_half_wrapped = np.array([half_half])

    perp1 = calc_perplexity(half_half_wrapped)
    perp2 = perplexity_row(half_half)

    print("FIRST PERPLEXITY", perp1)
    print("SECOND PERPLEXITY", perp2)

    assert np.isclose(calc_perplexity(half_half_wrapped), perplexity_row(half_half))

def test_induce_probabilities():
    """
    """
    distances = np.array([0, 10, 10])
    from src.t_sne import induce_probabilities
    distances_matrix = -1 * np.array([
        [0, 10, 10],
        [10, 0, 10],
        [0, 0, 10]])

    from src.tsne_ref_2 import calc_prob_matrix

    probabilities = induce_probabilities(distances, 1, 0.5)
    reference_probabilities = calc_prob_matrix(distances_matrix, sigmas=np.array([0.5]), zero_index=1)

    print(" PROBABILITIES ", probabilities)
    print(" REFERENCE_PROBABILITIES ", reference_probabilities)


"""
def compute_point_sigma():
    from src.t_sne import  compute_point_sigma
    I_100 = np.eye(100)
    D = vectorized_distance_matrix(I_100)
    D_0 = D[0]
    compute_point_sigma(D_0, 2,  )
    pass
"""
