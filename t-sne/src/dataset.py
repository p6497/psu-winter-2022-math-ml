import numpy as np
import scipy as s
import sklearn as sk
import matplotlib.pyplot as plt

from sklearn import manifold, datasets

def swissroll(n_samples=1500, noise=0):
    X, color = datasets.make_swiss_roll(n_samples)
    return X, color