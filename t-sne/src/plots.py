import os
import numpy as np
import scipy as s
import sklearn as sk
import matplotlib.pyplot as plt
import dataset
import t_sne
from pathlib import Path

ROOT = Path(__file__).parent.parent

def plot_swissroll_high_noise():
   plot_swissroll(noise=10)

def plot_swissroll_medium_noise():
   pass

def plot_swissroll_low_noise():
   pass

def plot_swissroll(noise=0, n_samples=10000):
   X, color = dataset.swissroll(n_samples=n_samples, noise=noise)
   fig = plt.figure()
   ax = fig.add_subplot(projection="3d")
   ax.scatter(X[:, 0], X[:, 1], X[:, 2], c=color, cmap=plt.cm.Spectral, s=10)
   plt.show()

def plot_tsne_reference():
   learning_rate=50
   perplexity=3
   n_samples=5000
   X, color = dataset.swissroll(n_samples=n_samples)
   fig = plt.figure()
   ax = fig.add_subplot()
   Y = t_sne.reference_tsne(X, perplexity=perplexity, learning_rate=learning_rate)
   ax.scatter(Y[:, 0], Y[:, 1], c=color, cmap=plt.cm.Spectral, s=10)

   ax.axis("tight")
   plt.title('T-SNE: Swissroll n_samples=%d perplexity=%d, learning_rate=%d' % (n_samples, perplexity, learning_rate))
   file_name = 'tsne_swiss_roll_reference_n_samples_%d_perplexity_%d_learning_rate_%d' % (n_samples, perplexity, learning_rate)
   save_file = os.path.realpath(os.curdir+'/../images/'+file_name)
   plt.savefig(save_file)
   print('done-saving')


