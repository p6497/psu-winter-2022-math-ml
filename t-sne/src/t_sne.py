import math

import numpy as np
import scipy as s
import sklearn as sk
import matplotlib.pyplot as plt

from sklearn import manifold

def compute_original_similairty(X, i, j):
    """ Compute the similarity of j with i centered on X. """
    x_i = X[i]
    for i in range(X):
        np.linalg.norm(X[i])
    return x_i

def reference_tsne(X, perplexity=30, learning_rate='auto'):
    """ """
    runner = manifold.TSNE(init="pca", random_state=0, perplexity=perplexity, learning_rate=learning_rate)
    Y = runner.fit_transform(X)
    return Y

def vectorized_distance_matrix(X):
    """
    This is concise way of creating the vecetorized distance
    matrix in high-dimensional space using the dataset X
    X: (n, d)
    1. Create a matrix with dot_product matrix
            square_X = [[ x_11*x_11,  x_12*x_12 .... x_1d*x1d],
                        [ x_21*x_21, ..., x_2d*x_2d],
                        ...
                        [ x_n1*x_n1, ..., x_nd*x_nd], ]
    1. Add Along the axis:
            sum_X    = [[ x_11**2 + x_12**2 + ...+ x_1d**2 ]]
    2.
    """
    square_X = np.square(X)
    sum_X = np.sum(square_X, 1)
    D = np.add(np.add(-2 * (X @ X.T), sum_X).T, sum_X)
    return D

def simple_distance_matrix(X):
    """
    This is a naive implementation of the distance matrix. It computes the
    pair wise euclidean distance square between each entry i, j.

    1. Thus the matrix is symmetric
    """
    n, d = X.shape
    distance_matrix = np.zeros((n, n))
    for i in range(n):
        a = X[i]
        for j in range(n):
            b = X[j]
            dist = 0
            for m in range(d):
                dist += (a[m] - b[m])**2
            computed_distance = dist
            np_dist = np.square(np.linalg.norm(a - b))
            assert np.isclose(computed_distance, np_dist)
            distance_matrix[i, j] = np_dist
    return distance_matrix


def perplexity(probability_matrix, i):
    """
    Compute the value of perplexity for a fixed data point.
    Perplixity can be thought of as a smooth measure of the
    effective number of neighbors of an element e_i.

    """
    H_i = 0
    n, _ = probability_matrix.shape
    neighbors_probabilities = probability_matrix[i]
    for p_ji in neighbors_probabilities:
        if p_ji == 0:
            continue
        H_i += p_ji*np.log2(p_ji)
    return np.exp2(-H_i)

def perplexity_row(induced_probabilities):
    """Perplexity of a given row."""
    # TODO: vectorize this.
    H = 0
    for p in induced_probabilities:
        if p == 0:
            continue
        H += p * np.log2(p)
    return np.exp2(-H)

def induce_probabilities(distance_row, column_index, sigma_guess):
    """For a given distance row we need to induce the probabilties."""
    two_sigma_squared = (2 * (sigma_guess ** 2))
    exp_sum = np.sum(np.exp(-1*distance_row) / two_sigma_squared )
    exp_cur = np.exp(-1*distance_row[column_index]/two_sigma_squared)
    induced_probabilities = np.empty(len(distance_row))
    for i in range(len(distance_row)):
        induced_probabilities[i] = exp_cur / exp_sum
    return induced_probabilities

"""
def simple_find_optimal_sigma(distances):
    pass
"""
def compute_point_sigma(distance_row, column_index, target_perplixity=30):
    """
    perform binary search for best sigam
    """
    sigma_min = 1e-5
    sigma_max = 1e5
    max_iter = 10000
    iter_count = 0
    sigma_guess = None
    print("distance row:", distance_row)

    while (sigma_min < sigma_max) or iter_count > max_iter:
        sigma_guess = (sigma_min + sigma_max) / 2.00

        induced_row_probability = induce_probabilities(distance_row,  column_index, sigma_guess)
        computed_perplexity = perplexity_row(induced_row_probability)

        if np.isclose(computed_perplexity - target_perplixity, 0):
            print("Found target perplexity", computed_perplexity, "target perplexity ", target_perplixity)
        elif computed_perplexity > target_perplixity:
            sigma_max = sigma_guess
        else:
            sigma_min = sigma_guess
        print("iter_count", iter_count,"sigma_guess", sigma_guess, "probability", computed_perplexity)
        iter_count += 1
    if iter_count == max_iter:
        print("WARNING: Did not find value but gave up.")
    return sigma_guess

def joint_neighbor_probability_matrix(X, target_perplexity=5):
    """
    Represents the probability matrix for the matrix
    :return:
    """
    n, d = X.shape
    # 1. Compute distance matrix
    D = vectorized_distance_matrix(X)
    # 2. Compute ideal sigmas to hit perplexity target.
    for i in range(n):
        compute_point_sigma(D[i], target_perplexity)
    pass

def joint_neighbor_probability_matrix_map(Y):
    """
    Represents the probability matrix for the map, The points
    in the map will move so as to minimize the cost function
    :return:
    """
    pass

def row_probabilities_for_sigma(squared_distances, sigma):
    # squared_distances
    return []

def row_perplexity(sigma, neighbor_probabilities):
    ## we need to induce a different probability distribuiton
    ## based on the sigma we are checking.
    H = 0
    for p_ji in neighbor_probabilities:
        if p_ji == 0:
            continue
        H += p_ji * np.log2(p_ji)
    _perplexity = np.exp2(-H)
    return _perplexity

def calc_perplexity(prob_matrix):
    """
        Calculate the perplexity of each row
        of a matrix of probabilities.
    """
    entropy = -np.sum(prob_matrix * np.log2(prob_matrix), 1)
    perplexity = 2 ** entropy
    return perplexity

## Useful Predicates
def is_symmetric_matrix(M):
    """
    Checks that every entry in M is the same as M^{T} thus
    making sure that the matrix is symmetric.
    """
    return (M == M.T).all()

