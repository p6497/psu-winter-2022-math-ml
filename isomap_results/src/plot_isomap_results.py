from sklearn import manifold
from sklearn import datasets
import matplotlib.pyplot as plt
from sklearn.manifold import Isomap # for Isomap dimensionality reduction

from sklearn.datasets import load_digits # for MNIST data
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import confusion_matrix,accuracy_score
from sklearn.decomposition import PCA

from matplotlib import pyplot

def get_mnist():
    digits = load_digits()
    X, y = load_digits(return_X_y=True)
    print('Shape of digit images: ', digits.images.shape)
    print('Shape of X (training data): ', X.shape)
    print('Shape of y (true labels): ', y.shape)
    return digits, X, y

def knn_classify(X, y):

    X_train, X_test, y_train, y_test = train_test_split(X, y,
                                                        test_size=0.20,
                                                        random_state=0)
    classifier = KNeighborsClassifier(n_neighbors=5, metric='euclidean', p=2)
    classifier.fit(X_train, y_train)
    y_pred = classifier.predict(X_test)
    cm = confusion_matrix(y_test, y_pred)
    ac = accuracy_score(y_test, y_pred)
    return ac, cm

def pca_dimensionality_reduction(X, y, d=56):
    """
    PCA for dimensionality reduction.
    """
    pca = PCA()
    pca.fit(X)
    transformed = pca.transform(X)
    transformed = transformed[:, 0:d]
    return transformed

def isomap_dimenionality_reduction(X, y, d=56):
    isomap = Isomap(n_components=d)
    isomap.fit(X)
    transformed = isomap.transform(X)
    transformed = transformed[:, 0: d]
    return transformed

def accuracy_by_dimensions(dimension_reduece_func, X, y, start=1, stop=64):
    """
    Get accuracy values
    """
    accuracies = []
    confusions = []

    for keep_dimensions in range(start, stop):
        d = keep_dimensions
        X_reduced = dimension_reduece_func(X, y, d)
        accuracy, confusion = knn_classify(X_reduced, y)
        accuracies.append(accuracy)
        confusions.append(confusion)
    return accuracies, confusions

