import numpy as np
import matplotlib.pyplot as plt

from src.plot_isomap_results import pca_dimensionality_reduction
from src.plot_isomap_results import isomap_dimenionality_reduction
from src.plot_isomap_results import knn_classify
from src.plot_isomap_results import get_mnist
from src.plot_isomap_results import accuracy_by_dimensions

def test_prediction_accuracy():
    digits, X, y = get_mnist()
    accuracy = knn_classify(X, y)
    print("ACCURACY", accuracy)

def test_pca_dimensionality_reduction():
    digits, X, y = get_mnist()
    pca_dimensionality_reduction(X, y)

def test_plot_accuracy_by_dimensions():
    digits, X, y = get_mnist()
    accuracies = []
    confusions = []
    for keep_dimensions in range(1, 64):
        d = keep_dimensions
        X_t = pca_dimensionality_reduction(X, y, d)
        accuracy, confusion = knn_classify(X_t, y)
        accuracies.append(accuracy)
        confusions.append(confusion)
    print("ACCURACY: ", accuracies)

def test_isomap_dimensionality_reduction():
    digits, X, y = get_mnist()
    accuracies, confusions = accuracy_by_dimensions(isomap_dimenionality_reduction, X, y, start=1, stop=64)


def test_plot_dimensionality_reduction_curves():
    """
    Something to do with plotting pass
    """
    digits, X, y = get_mnist()
    fig = plt.figure()
    ax = fig.add_subplot()
    min_dim = 1
    max_dim = 20
    dimensions = range(min_dim, max_dim)
    accuracies, confusions = accuracy_by_dimensions(pca_dimensionality_reduction, X, y, start=min_dim, stop=max_dim)
    plt.scatter(dimensions, accuracies, s=5, color='r')
    plt.plot(dimensions, accuracies)
    plt.title('KNN Classification Accuracy By Dimensions retained %s on MNIST' % 'PCA')
    # np.polyfit(dimensions, accuracies, 2, rcond=None, full=False, w=None, cov=False)

    plt.ylabel('Classification Accuracy')
    plt.xlabel('Dimensions')
    plt.show()
    fig.savefig('knn_accuracy_by_dimensionality.png')

def test_plot_dimensionality_reduction_curves_isomap():
    """
    Something to do with plotting pass
    """
    digits, X, y = get_mnist()
    fig = plt.figure()
    ax = fig.add_subplot()
    min_dim = 1
    max_dim = 20
    dimensions = range(min_dim, max_dim)
    accuracies, confusions = accuracy_by_dimensions(isomap_dimenionality_reduction, X, y, start=min_dim, stop=max_dim)
    plt.scatter(dimensions, accuracies, s=5, color='r')
    plt.plot(dimensions, accuracies)

    plt.title('KNN Classification Accuracy By Dimensions retained %s on MNIST' % 'Isomap')
    # np.polyfit(dimensions, accuracies, 2, rcond=None, full=False, w=None, cov=False)

    plt.ylabel('Classification Accuracy')
    plt.xlabel('Dimensions')
    plt.show()
    fig.savefig('knn_accuracy_by_dimensionality_isomap.png')
