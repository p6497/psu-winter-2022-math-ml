from sklearn import manifold
from sklearn import datasets
import matplotlib.pyplot as plt
from sklearn.manifold import Isomap # for Isomap dimensionality reduction
import os
from sklearn.datasets import load_digits # for MNIST data
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import confusion_matrix,accuracy_score
from sklearn.decomposition import PCA

from matplotlib import pyplot
from isomap import isomap
def get_mnist():
    digits = load_digits()
    X, y = load_digits(return_X_y=True)
    print('Shape of digit images: ', digits.images.shape)
    print('Shape of X (training data): ', X.shape)
    print('Shape of y (true labels): ', y.shape)
    return digits, X, y

def knn_classify(X, y):

    X_train, X_test, y_train, y_test = train_test_split(X, y,
                                                        test_size=0.20,
                                                        random_state=0)
    classifier = KNeighborsClassifier(n_neighbors=5, metric='euclidean', p=2)
    classifier.fit(X_train, y_train)
    y_pred = classifier.predict(X_test)
    cm = confusion_matrix(y_test, y_pred)
    ac = accuracy_score(y_test, y_pred)
    return ac, cm

def pca_dimensionality_reduction(X, y, d=56):
    """
    PCA for dimensionality reduction.
    """
    pca = PCA()
    pca.fit(X)
    transformed = pca.transform(X)
    transformed = transformed[:, 0:d]
    return transformed

def isomap_dimenionality_reduction(X, y, d=56):
    isomap = Isomap(n_components=d)
    isomap.fit(X)
    transformed = isomap.transform(X)
    transformed = transformed[:, 0: d]
    return transformed

def custom_isomap(X, y, d=56):
    pca = PCA()
    pca.fit(X)
    transformed = pca.transform(X)
    # transformed = transformed[:, 0:d]

    X_transformed = isomap(transformed,20,d).T
    return X_transformed


def accuracy_by_dimensions(dimension_reduce_func, X, y, start=1, stop=64):
    """
    Get accuracy values
    """
    accuracies = []
    confusions = []
    for keep_dimensions in range(start, stop):
        d = keep_dimensions
        X_reduced = dimension_reduce_func(X, y, d)
        # print(X_reduced.shape)
        accuracy, confusion = knn_classify(X_reduced, y)
        accuracies.append(accuracy)
        confusions.append(confusion)
    return accuracies, confusions


def plot_dimensionality_reduction_overall():
    digits, X, y = get_mnist()
    min_dim = 1
    max_dim = 20
    custom_isomap(X,y,1)
    dimensions = range(min_dim, max_dim)
    accuracies_pca, confusions_pca = accuracy_by_dimensions(pca_dimensionality_reduction, X, y, start=min_dim, stop=max_dim)
    accuracies_iso, confusions_iso = accuracy_by_dimensions(isomap_dimenionality_reduction, X, y, start=min_dim, stop=max_dim)
    accuracies_custom_iso, confusions_custom_iso = accuracy_by_dimensions(custom_isomap, X, y, start=min_dim, stop=max_dim)

    plt.scatter(dimensions,accuracies_pca, s=5, color='g',marker="v",label="PCA")
    plt.plot(dimensions,accuracies_pca)
    plt.scatter(dimensions, accuracies_iso , s=5, color='r',label="Isomap Standard")
    plt.plot(dimensions,accuracies_iso,color="g")
    plt.scatter(dimensions, accuracies_custom_iso , s=5, color='r',marker="s",label="Custom Standard")
    plt.plot(dimensions,accuracies_custom_iso,color="g")
    plt.title("Accuracy Comparison")
    plt.legend()
    image_path = os.path.realpath(os.path.join(os.path.dirname(__file__),"..","images","combined_accuracy_plot.png"))
    file_name = os.path.realpath(os.path.dirname(__file__ + '/../../../images/combined_accuracy_plot.png'))
    plt.savefig(image_path)

plot_dimensionality_reduction_overall()