import numpy as np
import matplotlib.pyplot as plt
from datasets import swissroll
from isomap import isomap
import time

#%% default data parameters
k = 10
n_samples = 2000
sigma = 0.1

plt.rcParams.update({'font.size': 12})

#%% function definitions
def plot_swissroll(n_samples, k, sigma, showRoll=True, save=False):

    X0, color = swissroll(noise=sigma, n_samples=n_samples)
    
    if showRoll:
        fig = plt.figure()
        ax = fig.add_subplot(projection="3d")
        ax.scatter(X0[:, 0], X0[:, 1], X0[:, 2], c=color, cmap=plt.cm.Spectral, s=10)
        ax.set_title(f"Swiss Roll: N={n_samples}, k={k}, $\sigma$={sigma}")
        plt.show()

    X = isomap(X0, k, 2)

    fig = plt.figure()
    ax = fig.add_subplot()
    ax.scatter(X[0,:], X[1, :], c=color, cmap=plt.cm.Spectral, s=10)
    ax.set_title(f"ISOMAP: N={n_samples}, k={k}, $\sigma$={sigma}")
    if save:
        plt.savefig(f'../images/SRplots_N{n_samples}_k{k}.png')
    
def plot_sr_grid_nk(n_samples, k, ax):
    X0, color = swissroll(noise=sigma, n_samples=n_samples)
    X = isomap(X0, k, 2)

    ax.scatter(X[0,:], X[1, :], c=color, cmap=plt.cm.Spectral, s=10)
    ax.set_xlabel(f"k={k}")
    ax.set_ylabel(f"N={n_samples}")
    ax.set_xticks([])
    ax.set_yticks([])
    
def plot_sr_grid_s(sig, axes, j):
    X0, color = swissroll(noise=sig, n_samples=2000)
    X = isomap(X0, 20, 2)
    
    sr_ax = axes[0][j]
    iso_ax = axes[1][j]
    
    sr_ax.scatter(X0[:, 0], X0[:, 1], X0[:, 2], c=color, cmap=plt.cm.Spectral, s=4.0)
    if j == 3:
        sr_ax.set_zlabel("Swiss Roll")
    
    iso_ax.scatter(X[0,:], X[1,:], np.zeros_like(X[0,:]), c=color, cmap=plt.cm.Spectral, s=2.0)
    if j == 3:
        iso_ax.set_zlabel("ISOMAP")
    iso_ax.set_xlabel(f"$\sigma={sig}$")
   
    sr_ax.set_xticks([])
    sr_ax.set_yticks([])
    sr_ax.set_zticks([])
    iso_ax.set_xticks([])
    iso_ax.set_yticks([])
    iso_ax.set_zticks([])
    
def test_time(X0):
    t0 = time.perf_counter()
    isomap(X0, 10, 2) # (use 10 neighbors)
    t1 = time.perf_counter()
    return t1 - t0
    
def plot_times(sample_counts):
    times = []
    for sample_count in sample_counts:
        print(f'timing {sample_count}')
        X0, color = swissroll(noise=sigma, n_samples=sample_count)
        times.append(test_time(X0))

    (d,c,b,a) = np.polyfit(sample_counts, times, deg=3)

    lbf = [a + b*x + c*x**2 + d* x**3 for x in sample_counts]

    fig = plt.figure()
    ax = fig.add_subplot()
    ax.plot(sample_counts, times, alpha=0.75, label="Exec. Time")
    ax.plot(sample_counts, lbf, alpha=0.5, label=f"${d:.3e}N^3$")
    ax.set_ylabel('Time (s)')
    ax.set_xlabel('N')
    ax.set_title("Execution Time vs. Problem Size")
    plt.legend()
    plt.show()
    
#%% Performance varying size and k Grid
sizes = [500,1000,2000]
ks = [5,10,20]
fig, axes = plt.subplots(3, 3)
for i, size in enumerate(sizes):
    for j, kv in enumerate(ks):
        print(i, j)
        plot_sr_grid_nk(size, kv, axes[i][j])
for ax in axes.flat:
    ax.label_outer()
plt.suptitle("Swiss Roll Performance With Varying N and K")
plt.show()

# individual plots
for i, size in enumerate(sizes):
    for j, kv in enumerate(ks):
        plot_swissroll(size, kv, sigma)


#%% Performance varying noise
plt.rcParams.update({'font.size': 22})
sigmas = [0.0, .5, 1.0, 1.5]
fig, axes = plt.subplots(2, 4, figsize=(15,7.5), subplot_kw=dict(projection='3d'))
for j, sig in enumerate(sigmas):
    print(j)
    plot_sr_grid_s(sig, axes, j)
for ax in axes.flat:
    ax.label_outer()
plt.suptitle("Swiss Roll Performance With Varying Sigma (N=2000, k=20)")
plt.show()

#%% Performance varying parameters
ks = [5,10,20,50]
for kval in ks:
    plot_swissroll(n_samples, kval, sigma, showRoll=False)

#%% Plot varying samples by time
plot_times(np.linspace(50, 2000, 20).astype(int))