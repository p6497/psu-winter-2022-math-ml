import numpy as np

def neighborhoodGraph(data, k=5):
    D = vectorized_distance_matrix(data)
    Graph = np.copy(D)
    for i in range(D.shape[0]):
        row = D[i]
        pivot = np.partition(row, k)[k]
        for j in range(D.shape[1]):
            value = row[j]
            if value > pivot: Graph[i][j] = np.infty
    return Graph


def vectorized_distance_matrix(X):
    """
    This is concise way of creating the vecetorized distance
    matrix in high-dimensional space using the dataset X
    X: (n, d)
    1. Create a matrix with dot_product matrix
            square_X = [[ x_11*x_11,  x_12*x_12 .... x_1d*x1d],
                        [ x_21*x_21, ..., x_2d*x_2d],
                        ...
                        [ x_n1*x_n1, ..., x_nd*x_nd], ]
    1. Add Along the axis:
            sum_X    = [[ x_11**2 + x_12**2 + ...+ x_1d**2 ]]
    2.
    """
    square_X = np.square(X)
    sum_X = np.sum(square_X, 1)
    D = np.add(np.add(-2 * (X @ X.T), sum_X).T, sum_X)
    D[np.where(np.isclose(D, 0))] = 0
    return np.sqrt(D)