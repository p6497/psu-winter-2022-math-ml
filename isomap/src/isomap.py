import numpy as np
from scipy.sparse import csr_matrix
from scipy.sparse.csgraph import floyd_warshall
from isomap_helpers import neighborhoodGraph

largeNum = np.infty

#%% Isomap definition
def isomap(X0, k, d):
    # nearest neighbor graph
    Graph = neighborhoodGraph(X0, k)
    
    # geodesic distance matrix
    D = floyd_warshall(csr_matrix(Graph), directed=False)
    D[np.where(D == np.infty)] = largeNum
    
    # Center distance matrix
    n = X0.shape[0]
    H = np.eye(n) - 1/n * np.ones((n,n)) # Centering matrix
    G = -1/2 * H @ (D*D) @ H             # Gram data matrix
    
    # Compute lower dimensional data
    Lambda, V = np.linalg.eigh(G)
    Lambda, V = Lambda[:-d-1:-1],  V[:,:-d-1:-1] # get d largest evals/vecs
    X = np.diag(np.sqrt(Lambda)) @ V.T
    
    return X
