import math
import numpy as np

from sklearn import datasets

def swissroll(n_samples=1500, noise=0):
    X, color = datasets.make_swiss_roll(n_samples, noise=noise)
    return X, color

def generate_swissroll(n, radius=20):
    z = np.random.randn(2, n)
    # z[0] = np.sqrt(z[0])
    # z = 3 * math.pi * z
    r = z[0] + radius
    x = np.vstack([
        r*np.cos(z[0]),
        r*np.sin(z[0]),
        z[1]])
    return x, z

def vectorized_distance_matrix(X):
    """
    This is concise way of creating the vecetorized distance
    matrix in high-dimensional space using the dataset X
    X: (n, d)
    1. Create a matrix with dot_product matrix
            square_X = [[ x_11*x_11,  x_12*x_12 .... x_1d*x1d],
                        [ x_21*x_21, ..., x_2d*x_2d],
                        ...
                        [ x_n1*x_n1, ..., x_nd*x_nd], ]
    1. Add Along the axis:
            sum_X    = [[ x_11**2 + x_12**2 + ...+ x_1d**2 ]]
    2.
    """
    square_X = np.square(X)
    sum_X = np.sum(square_X, 1)
    D = np.add(np.add(-2 * (X @ X.T), sum_X).T, sum_X)
    return D